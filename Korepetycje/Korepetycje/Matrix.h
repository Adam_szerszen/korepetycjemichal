﻿#pragma once
#include <stdexcept>
#include <ostream>

using std::out_of_range;
using std::invalid_argument;
using std::ostream;

class Matrix
{
private:
	int columns;
	int rows;

	int** matrix;
	void createMatrixArray();
	void setDefaultValues() const;
public:
	Matrix();
	Matrix(int columns, int rows);
	Matrix(const Matrix& matrix);
	int Columns()const;
	int Rows()const;
	int At(int column, int row) const;
	void Set(int column, int row, int value) const;

	Matrix operator+ (const Matrix& matrix) const;
	Matrix operator- (const Matrix& matrix) const;
	Matrix operator* (const Matrix& matrix) const;
	Matrix operator* (const int scalar) const;
	Matrix* operator= (const Matrix& matrix);
	int operator() (const int column, const int row)const;
	bool operator== (const Matrix& matrix) const;
	bool operator!= (const Matrix& matrix) const;
	Matrix* operator+= (const Matrix& matrix);
	Matrix* operator-= (const Matrix& matrix);
	Matrix* operator*= (const int scalar);
	Matrix* operator- ();
	friend ostream& operator<< (ostream& stream, const Matrix& matrix);
	~Matrix();
};