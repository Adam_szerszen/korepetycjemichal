﻿#include <iostream>
#include "Matrix.h"

using std::cout;
using std::endl;

int main()
{
	auto A = new Matrix(3, 2);
	
	A->Set(0, 0, 1);
	A->Set(1, 0, 0);
	A->Set(2, 0, 2);
	A->Set(0, 1, -1);
	A->Set(1, 1, 3);
	A->Set(2, 1, 1);
	cout << *A << endl;
	auto B = new Matrix(2, 3);

	B->Set(0, 0, 3);
	B->Set(1, 0, 1);
	B->Set(0, 1, 2);
	B->Set(1, 1, 1);
	B->Set(0, 2, 1);
	B->Set(1, 2, 0);
	cout << *B << endl;
	auto result = *A * *B;
	cout << result << endl;
	cout << result * 2 << endl;
	cout << result(0, 1) << endl;

	if (*A == *B)
	{
		cout << "Rowne!" << endl;
	}
	else
	{
		cout << "Nie rowne..." << endl;
		if (*A == *A)
		{
			cout << "To samo!" << endl;
			
		}
	}

	auto C = Matrix(3, 2);

	C.Set(0, 0, 1);
	C.Set(1, 0, 0);
	C.Set(2, 0, 2);
	C.Set(0, 1, -1);
	C.Set(1, 1, 3);
	C.Set(2, 1, 1);

	auto D = new Matrix(3, 2);

	D->Set(0, 0, 2);
	D->Set(1, 0, 12);
	D->Set(2, 0, 7);
	D->Set(0, 1, -6);
	D->Set(1, 1, 11);
	D->Set(2, 1, 76);

	*D = C;
	cout << *D << endl;
	*D += *D;
	*D += *D;
	*D += *D;
	*D += *D;
	cout << *D << endl;
	*D *= 3;
	cout << *D << endl;

	auto G = -*D;
	cout << *G << endl;
	auto H = new Matrix(*G);
	cout << *G << endl;
	//auto macierz = new Matrix(3, 3);
	//auto druga = new Matrix(3, 3);
	//macierz->Set(0, 0, 22);
	//druga->Set(2, 0, 12);
	//auto wynik = *macierz + *druga;
	//cout << wynik;
	//try
	//{
	//	cout << macierz->At(1, 1) << endl;
	//	macierz->Set(2, 0, 256);
	//	cout << macierz->At(2, 0) << endl;
	//	cout << macierz->At(-1, 12) << endl;	// ta linijka wyrzuci wyjatek outOfRange
	//}
	//catch (out_of_range)	// a to miejsce zlapie go gdy wystapi
	//{
	//	cout << "Index spoza rozmiaru macierzy!" << endl;	// i wykona wszystko co tu napiszemy
	//	cout << *macierz; 
	//}
	//catch (invalid_argument)
	//{
	//	cout << "Niewlasciwy argument operatora!" << endl;
	//}

	system("pause");
	delete A;
	delete B;
	return 0;
}
