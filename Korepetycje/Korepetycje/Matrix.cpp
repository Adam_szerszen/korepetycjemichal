﻿#include "Matrix.h"

void Matrix::createMatrixArray()
{
	matrix = new int*[rows];

	for (auto i = 0; i < rows; ++i)
	{
		matrix[i] = new int[columns];
	}
}

void Matrix::setDefaultValues() const
{
	for (auto i = 0; i < rows; ++i)
	{
		for (auto j = 0; j < columns; ++j)
		{
			matrix[i][j] = 0;
		}
	}
}

Matrix::Matrix() : columns(0), rows(0), matrix(nullptr)
{
}

Matrix::Matrix(int columns, int rows) : columns(columns), rows(rows)
{
	createMatrixArray();
	setDefaultValues();
}

Matrix::Matrix(const Matrix& matrix)
{
	columns = matrix.Columns();
	rows = matrix.Rows();
	createMatrixArray();
	setDefaultValues();

	for (auto i = 0; i < matrix.Rows(); i++)
	{
		for (auto j = 0; j < matrix.Columns(); j++)
		{
			auto matrixValue = matrix.At(j, i);
			this->Set(j, i, matrixValue);
		}
	}
}

int Matrix::Columns() const
{
	return columns;
}

int Matrix::Rows() const
{
	return rows;
}

int Matrix::At(int column, int row) const
{
	if (column < columns && column > -1 && row < rows && row > -1)
	{
		return matrix[row][column];
	}
	throw out_of_range("Index out of range!");
}

void Matrix::Set(int column, int row, int value) const
{
	if (column < columns && column > -1 && row < rows && row > -1)
	{
		matrix[row][column] = value;
	}
	else
	{
		throw out_of_range("Index out of range!");
	}
}

Matrix Matrix::operator+(const Matrix& matrix) const
{
	if (this->Rows() == matrix.Rows() && this->Columns() == matrix.Columns())
	{
		auto result = new Matrix(matrix.Columns(), matrix.Rows());
		for (auto i = 0; i < matrix.Rows(); i++)
		{
			for (auto j = 0; j < matrix.Columns(); j++)
			{
				result->Set(j, i, this->At(j, i) + matrix.At(j, i));
			}
		}
		return *result;
	}
	throw invalid_argument("Invalid operator argument!");
}

Matrix Matrix::operator-(const Matrix& matrix) const
{
	if (this->Rows() == matrix.Rows() && this->Columns() == matrix.Columns())
	{
		auto result = new Matrix(matrix.Columns(), matrix.Rows());
		for (auto i = 0; i < matrix.Rows(); i++)
		{
			for (auto j = 0; j < matrix.Columns(); j++)
			{
				result->Set(j, i, this->At(j, i) - matrix.At(j, i));
			}
		}
		return *result;
	}
	throw invalid_argument("Invalid operator argument!");
}

Matrix Matrix::operator*(const Matrix& matrix) const
{
	if (this->Columns() == matrix.Rows())
	{
		auto result = new Matrix(this->Rows(), matrix.Columns());

		for (auto i = 0; i < this->Rows(); ++i)
		{
			for (auto j = 0; j < matrix.Columns(); ++j) 
			{
				for (auto k = 0; k < this->Columns(); ++k)
				{
					auto tempValue = result->At(j, i);
					tempValue += this->At(k, i) * matrix.At(j, k);
					result->Set(j, i, tempValue);
				}
			}
		}
		return *result;
	}
	throw invalid_argument("Invalid operator argument!");
}

Matrix Matrix::operator*(const int scalar) const
{
	auto result = new Matrix(this->Columns(), this->Rows());
	for (auto i = 0; i < this->Rows(); i++)
	{
		for (auto j = 0; j < this->Columns(); j++)
		{
			result->Set(j, i, this->At(j, i) * scalar);
		}
	}
	return *result;
}

Matrix* Matrix::operator=(const Matrix& matrix)
{
	for (auto i = 0; i < matrix.Rows(); i++)
	{
		for (auto j = 0; j < matrix.Columns(); j++)
		{
			auto matrixValue = matrix.At(j, i);
			this->Set(j, i, matrixValue);
		}
	}
	return this;
}

int Matrix::operator()(const int column, const int row) const
{
	if (column < columns && column > -1 && row < rows && row > -1)
	{
		return matrix[row][column];
	}
	throw out_of_range("Index out of range!");
}

bool Matrix::operator==(const Matrix& matrix) const
{
	if (this->Rows() == matrix.Rows() && this->Columns() == matrix.Columns())
	{
		auto columns = matrix.Columns();
		auto rows = matrix.Rows();
		for (auto i = 0; i < rows; i++)
		{
			for (auto j = 0; j < columns; j++)
			{
				if (this->At(j, i) != matrix.At(j, i))
				{
					return false;
				}
			}
		}
		return true;
	}
	return false;
}

bool Matrix::operator!=(const Matrix& matrix) const
{
	if (this->Rows() == matrix.Rows() && this->Columns() == matrix.Columns())
	{
		auto columns = matrix.Columns();
		auto rows = matrix.Rows();
		for (auto i = 0; i < rows; i++)
		{
			for (auto j = 0; j < columns; j++)
			{
				if (this->At(j, i) != matrix.At(j, i))
				{
					return true;
				}
			}
		}
		return false;
	}
	return true;
}

Matrix* Matrix::operator+=(const Matrix& matrix)
{
	if (this->Rows() == matrix.Rows() && this->Columns() == matrix.Columns())
	{
		auto columns = matrix.Columns();
		auto rows = matrix.Rows();
		for (auto i = 0; i < rows; i++)
		{
			for (auto j = 0; j < columns; j++)
			{
				auto temp = this->At(j, i) + matrix.At(j, i);
				this->Set(j, i, temp);
			}
		}
		return this;
	}
	throw invalid_argument("Invalid operator argument!");
}

Matrix* Matrix::operator-=(const Matrix& matrix)
{
	if (this->Rows() == matrix.Rows() && this->Columns() == matrix.Columns())
	{
		auto columns = matrix.Columns();
		auto rows = matrix.Rows();
		for (auto i = 0; i < rows; i++)
		{
			for (auto j = 0; j < columns; j++)
			{
				auto temp = this->At(j, i) - matrix.At(j, i);
				this->Set(j, i, temp);
			}
		}
		return this;
	}
	throw invalid_argument("Invalid operator argument!");
}

Matrix* Matrix::operator*=(const int scalar)
{
	for (auto i = 0; i < this->Rows(); i++)
	{
		for (auto j = 0; j < this->Columns(); j++)
		{
			this->Set(j, i, this->At(j, i) * scalar);
		}
	}
	return this;
}

Matrix* Matrix::operator-()
{
	auto result = new Matrix(this->Rows(), this->Columns());
	for (auto i = 0; i < this->Columns(); i++)
	{
		for (auto j = 0; j < this->Rows(); j++)
		{
			result->Set(j, i, this->At(i, j));
		}
	}
	return result;
}

Matrix::~Matrix()
{
	if (columns > 0 && rows > 0)
	{
		if (matrix != nullptr)
		{
			for (auto i = 0; i < rows; ++i)
			{
				delete matrix[i];
			}

			delete matrix;
		}
	}
}

ostream& operator<<(ostream& stream, const Matrix& matrix)
{
	for (auto i = 0; i < matrix.Rows(); i++)
	{
		for (auto j = 0; j < matrix.Columns(); j++)
		{
			stream << matrix.At(j, i) << " ";
		}
		stream << "\n";
	}
	return stream;
}
