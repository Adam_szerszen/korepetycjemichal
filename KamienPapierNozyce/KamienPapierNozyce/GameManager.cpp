﻿#include "GameManager.h"

using std::cout;
using std::endl;

void GameManager::checkRoundResults()
{
	cout << playerOne->GetPlayerName() << " " << *playerOne << " vs " << playerTwo->GetPlayerName() << " " << *playerTwo << endl;

	if (*playerOne == *playerTwo)
	{
		cout << "DRAW!" << endl;
	}
	else
	{
		if (*playerOne < *playerTwo)
		{
			cout << "Player " << playerTwo->GetPlayerName() << " won this round!" << endl;
			playerTwoPoints++;
		}
		else
		{
			cout << "Player " << playerOne->GetPlayerName() << " won this round!" << endl;
			playerOnePoints++;
		}
	}
}

void GameManager::checkWinner()
{
	if (playerOnePoints == 3 || playerTwoPoints == 3)
	{
		gameStatus = gameFinished;

		if (playerOnePoints == 3)
		{
			cout << "Player " << playerOne->GetPlayerName() << " won game!" << endl;
		}
		else
		{
			cout << "Player " << playerTwo->GetPlayerName() << " won game!" << endl;
		}
	}
}

GameManager::GameManager(Player* playerOne, Player* playerTwo) :
	gameStatus(beforeGame),
	playerOnePoints(0),
	playerTwoPoints(0),
	playerOne(playerOne),
	playerTwo(playerTwo)
{
}

void GameManager::Play()
{
	gameStatus = gameInProgress;

	while (gameStatus == gameInProgress)
	{
		playerOne->SelectChoice();
		playerTwo->SelectChoice();
		checkRoundResults();
		checkWinner();
		system("pause");
	}
}

GameManager::~GameManager()
{
	delete playerOne;
	delete playerTwo;
}
