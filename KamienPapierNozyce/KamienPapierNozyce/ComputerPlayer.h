﻿#pragma once
#include "Player.h"
#include <random>

using std::rand;

class ComputerPlayer : public Player
{
public:
	explicit ComputerPlayer(const string& playerName);
	void SelectChoice() override;
};
