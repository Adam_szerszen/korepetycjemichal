﻿#pragma once
#include <iostream>
#include "Player.h"

using std::cout;
using std::cin;
using std::endl;
using std::stoi;

class HumanPlayer : public Player
{
public:
	explicit HumanPlayer(const string& playerName);

	void SelectChoice() override;
};
