﻿#pragma once
#include <iostream>
#include "Player.h"

class GameManager
{
private:
	GameStatus gameStatus;
	int playerOnePoints;
	int playerTwoPoints;

	Player* playerOne;
	Player* playerTwo;

	void checkRoundResults();
	void checkWinner();
public:
	GameManager(Player* playerOne, Player* playerTwo);
	void Play();
	~GameManager();
};
