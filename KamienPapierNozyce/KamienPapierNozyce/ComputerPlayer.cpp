﻿#include "ComputerPlayer.h"

ComputerPlayer::ComputerPlayer(const string& playerName): Player(playerName)
{
}

void ComputerPlayer::SelectChoice()
{
	auto choice = (rand() % 3) + 1;

	switch (choice)
	{
	case 1:
		currentChoice = Rock;
		break;
	case 2:
		currentChoice = Paper;
		break;
	case 3: 
		currentChoice = Scissors;
		break;
	default:
		currentChoice = Any;
	}
}
