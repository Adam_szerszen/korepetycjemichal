﻿#include "Player.h"

Player::Player(string playerName) : currentChoice(Any)
{
	this->playerName = playerName;
}

Choice Player::GetCurrentChoice() const
{
	return currentChoice;
}

string Player::GetPlayerName() const
{
	return playerName;
}

bool Player::operator==(const Player& player) const
{
	return this->currentChoice == player.GetCurrentChoice();
}

bool Player::operator<(const Player& player) const
{
	if (*this == player)
	{
		return false;
	}

	if (player.GetCurrentChoice() == Any)
	{
		throw invalid_argument("Invalid operator argument!");
	}

	switch (this->currentChoice)
	{
	case Rock:
		if (player.GetCurrentChoice() == Paper)
		{
			return true;
		}
		return false;

	case Paper:
		if (player.GetCurrentChoice() == Scissors)
		{
			return true;
		}
		return false;

	case Scissors:
		if (player.GetCurrentChoice() == Rock)
		{
			return true;
		}
		return false;
	default:
		throw invalid_argument("Invalid operator argument!");
	}
}

ostream& operator<<(ostream& stream, const Player& player)
{
	auto choice = player.currentChoice;
	auto choiceString = "";
	
	switch (choice)
	{
	case Rock:
		choiceString = "ROCK";
		break;
	case Paper:
		choiceString = "PAPER";
		break;
	case Scissors: 
		choiceString = "SCISSORS";
		break;
	default:
		throw invalid_argument("Invalid operator argument!");
	}

	stream << choiceString;
	return stream;
}
