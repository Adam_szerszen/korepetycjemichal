#include "GameManager.h"
#include "ComputerPlayer.h"
#include "HumanPlayer.h"
#include <time.h>

int main()
{
	srand(time(NULL));

	auto computerOne = new ComputerPlayer("Amiga-3000B");
	auto computerTwo = new ComputerPlayer("Comodore-1999");
	
	auto playerOne = new HumanPlayer("Bolek");
	auto playerTwo = new HumanPlayer("Lolek");

	/*auto gameManager = new GameManager(playerOne, playerTwo);
	gameManager->Play();
	delete gameManager;*/

	auto gameManager = new GameManager(computerOne, computerTwo);
	gameManager->Play();
	delete gameManager;

	/*auto gameManager = new GameManager(playerOne, computerTwo);
	gameManager->Play();
	delete gameManager;*/

	return 0;
}
