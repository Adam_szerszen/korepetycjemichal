﻿#pragma once
#include "Models.h"
#include <stdexcept>
#include <string>

using std::string;
using std::ostream;
using std::invalid_argument;

class Player
{
private:
	string playerName;
protected:
	Choice currentChoice;
public:
	explicit Player(string playerName);
	Choice GetCurrentChoice()const;
	string GetPlayerName()const;
	
	bool operator== (const Player& player) const;
	bool operator< (const Player& player) const;
	friend ostream& operator<< (ostream& stream, const Player& player);

	virtual void SelectChoice() =0;
	virtual ~Player() = default;
};
