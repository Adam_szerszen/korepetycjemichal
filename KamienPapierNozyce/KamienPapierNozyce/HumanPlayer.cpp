﻿#include "HumanPlayer.h"

HumanPlayer::HumanPlayer(const string& playerName) : Player(playerName)
{

}

void HumanPlayer::SelectChoice()
{
	string rawChoice = "";
	auto choice = 0;
	auto notFinished = true;

	system("cls");
	cout << this->GetPlayerName() << ": Please make your choice (1 - ROCK, 2 - PAPER, 3 - SCISSORS) : " << endl;
	while (notFinished)
	{
		cin >> rawChoice;
		if (rawChoice.length() > 1)
		{
			cout << "Wrong input! Number must be in range: 1, 2, 3" << endl;
		}
		else
		{
			try
			{
				choice = stoi(rawChoice);
				switch (choice)
				{
				case 1:
					currentChoice = Rock;
					notFinished = false;
					break;
				case 2:
					currentChoice = Paper;
					notFinished = false;
					break;
				case 3:
					currentChoice = Scissors;
					notFinished = false;
					break;
				default:
					cout << "Wrong input! Number must be in range: 1, 2, 3" << endl;
				}
			}
			catch (invalid_argument)
			{
				cout << "Wrong input! Number must be in range: 1, 2, 3" << endl;
			}
			
		}
		cin.clear();
		cin.ignore(INT_MAX, '\n');
	}
}
