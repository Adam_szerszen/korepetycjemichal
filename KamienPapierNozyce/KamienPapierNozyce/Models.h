#pragma once

enum Choice
{
	Any,
	Rock,
	Paper,
	Scissors
};

enum GameStatus
{
	beforeGame,
	gameInProgress,
	gameFinished
};
